<?php
/*
Plugin Name: Woocommerce Modo Catálogo
Plugin URI: http://www.3comunicacion.com
Description: Plugin no oficial que pone Woocomerce en modo "catálogo", es decir, deshabilita todas las compras
Version: 0.1
License: GPLv2 or later
Author: Aday Talavera
Author URI: http://www.felfo.com
Parent: woocommerce
*/

function woo_catalog_mode_hide_all() {
    return '';
}
add_filter('woocommerce_get_price_html', 'woo_catalog_mode_hide_all');
add_filter('woocommerce_loop_add_to_cart_link', 'woo_catalog_mode_hide_all');

function woo_catalog_remove_actions() {
    remove_action('woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart', 10);
    remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 30);
    remove_action('woocommerce_simple_add_to_cart', 'woocommerce_simple_add_to_cart', 30);
    remove_action('woocommerce_grouped_add_to_cart', 'woocommerce_grouped_add_to_cart', 30);
    remove_action('woocommerce_variable_add_to_cart', 'woocommerce_variable_add_to_cart', 30);
    remove_action('woocommerce_external_add_to_cart', 'woocommerce_external_add_to_cart', 30);
}
add_action('woocommerce_loaded', 'woo_catalog_remove_actions');
